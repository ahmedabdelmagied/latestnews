package com.example.abdelmagied.latestnews.utilites;

import android.net.Uri;
import android.os.AsyncTask;

import com.example.abdelmagied.latestnews.models.NewsClass;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import static android.net.Uri.parse;

/**
 * Created by abdelmagied on 20/11/17.
 */

public class NetworkUtilites {

    public static  String News_API = "https://newsapi.org/v2/top-headlines";

    public static URL buildSpecificNewsUrl(String Categorytype, String API_KEY) {

        if(Categorytype == "technology"){
            News_API = "https://newsapi.org/v2/everything";
        }
           Uri buildUri = parse(News_API).buildUpon()
                .appendQueryParameter("category", Categorytype)
                .appendQueryParameter("country", "eg")
                .appendQueryParameter("apiKey", API_KEY)
                .build();

        URL url = null;
        try {
            url = new URL(buildUri.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }


    public static String getResponseFromHttpRequest(URL url) throws IOException {

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream inputStream = urlConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream);
            scanner.useDelimiter("\\A");
            if (scanner.hasNext()) {
                return scanner.next();
            }

        } finally {
            urlConnection.disconnect();
        }
        return "Failed";
    }


    public static ArrayList<NewsClass> getSpecificNews(String CategoryType, String API_KEY) throws IOException, ExecutionException, InterruptedException, JSONException {

        URL myurl = buildSpecificNewsUrl(CategoryType, API_KEY);
        String Jsonstring = new getRequestFromHttp().execute(myurl).get();
        ArrayList<NewsClass> newsSpecified = JsonUtilites.readJsonData(Jsonstring, CategoryType);
        return newsSpecified;

    }


    static class getRequestFromHttp extends AsyncTask<URL, Void, String> {
        @Override
        protected String doInBackground(URL... urls) {
            URL myurl = urls[0];
            String q = "";
            try {
                q = getResponseFromHttpRequest(myurl);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return q;
        }
    }
}
