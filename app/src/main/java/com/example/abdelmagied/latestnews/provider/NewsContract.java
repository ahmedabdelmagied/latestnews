package com.example.abdelmagied.latestnews.provider;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by abdelmagied on 22/11/17.
 */

public class NewsContract {

    public static final String AUTHORITY = "com.example.abdelmagied.latestnews";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + AUTHORITY);

    public static final String NEWS_PATH = "news";

    public static final String NEWS_DATABASE_NAME = "mydatabase";
    public static final String[] COLUMN_REQUIRED =
            {
                    NewsEntry.COLUMN_AUTHOR,
                    NewsEntry.COLUMN_TITLE,
                    NewsEntry.COLUMN_DESCRIPTION,
                    NewsEntry.COLUMN_PUBLISHEDAT,
                    NewsEntry.COLUMN_TYPE,
                    NewsEntry.COLUMN_URLTOIMAGE
            };


    public static final class NewsEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(NEWS_PATH).build();

        public static final String ROOT_TABLE_NAME = "rootah";
        public static final String COLUMN_AUTHOR = "author";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_URL = "url";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_URLTOIMAGE = "urlToImage";
        public static final String COLUMN_PUBLISHEDAT = "published";
        public static final String COLUMN_TYPE = "Categorytype";
        public static final String COLUMN_WIDGET = "inWidget";


    }
}
