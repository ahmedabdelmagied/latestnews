package com.example.abdelmagied.latestnews.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by abdelmagied on 22/11/17.
 */

public class NewsProvider extends ContentProvider {

    public static final int NEWS = 100;
    public static final int NEWS_WITH_ID = 101;
    public static final UriMatcher mUriMatcher = buildUriMatcher();
    public static final String ACTION_UPDATA_NEWS_WIDGET = "com.example.abdelmagied.latestnews.WIDGET_UPDATA";
    public static Context context;
    public NewsDB mNewsDb;

    public static UriMatcher buildUriMatcher() {

        UriMatcher urimatcher = new UriMatcher(UriMatcher.NO_MATCH);
        urimatcher.addURI(NewsContract.AUTHORITY, NewsContract.NEWS_PATH, NEWS);
        urimatcher.addURI(NewsContract.AUTHORITY, NewsContract.NEWS_PATH + "/#", NEWS_WITH_ID);

        return urimatcher;
    }

    @Override
    public boolean onCreate() {
        this.context = getContext();
        mNewsDb = new NewsDB(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        SQLiteDatabase sqlitedatabase = mNewsDb.getReadableDatabase();
        int matcher = mUriMatcher.match(uri);
        Cursor mycursor;
        switch (matcher) {
            case NEWS:
                mycursor = sqlitedatabase.query(NewsContract.NewsEntry.ROOT_TABLE_NAME,
                        strings,
                        s,
                        strings1,
                        null,
                        null,
                        s1);
                break;
            case NEWS_WITH_ID:
                mycursor = sqlitedatabase.query(NewsContract.NewsEntry.ROOT_TABLE_NAME,
                        strings,
                        "_id = ?",
                        new String[]{uri.getPathSegments().get(1)},
                        null,
                        null,
                        s1);
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri " + uri);
        }
        mycursor.setNotificationUri(getContext().getContentResolver(), uri);
        
        return mycursor;

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase db = mNewsDb.getWritableDatabase();
        int matcher = mUriMatcher.match(uri);
        Uri rUri = null;
        switch (matcher) {
            case NEWS:
                long insertedId = db.insert(NewsContract.NewsEntry.ROOT_TABLE_NAME, null, contentValues);

                if (insertedId > 0) {
                    rUri = ContentUris.withAppendedId(NewsContract.NewsEntry.CONTENT_URI, insertedId);
                } else {
                    throw new android.database.SQLException("failed to insert " + uri);
                }
                break;
            default:
                throw new UnsupportedOperationException("Unknown Uri :  " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        Intent newsUpdataIntent = new Intent(ACTION_UPDATA_NEWS_WIDGET).setPackage(getContext().getPackageName());
        getContext().sendBroadcast(newsUpdataIntent);
        return rUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int numRowsDeleted;
        if (null == s) s = "1";
        switch (mUriMatcher.match(uri)) {
            case NEWS:
                numRowsDeleted = mNewsDb.getWritableDatabase().delete(
                        NewsContract.NewsEntry.ROOT_TABLE_NAME,
                        s,
                        strings);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        if (numRowsDeleted != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return numRowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }
}
