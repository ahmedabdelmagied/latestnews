package com.example.abdelmagied.latestnews;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.abdelmagied.latestnews.models.NewsClass;
import com.example.abdelmagied.latestnews.provider.NewsContract;

/**
 * Created by abdelmagied on 03/12/17.
 */

public class GridWidgetService extends RemoteViewsService {
    @Override
    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new GridRemoteViewsFactory(this.getApplicationContext() , intent);
    }
}

class GridRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;
    private Cursor mCursor;

    public GridRemoteViewsFactory(Context context , Intent intent){
        this.mContext = context;
    }

    @Override
    public void onCreate() {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onDataSetChanged() {
        if (mCursor != null){
            mCursor.close();
            mCursor = null;
        }

        mCursor = mContext.getContentResolver().query(
                NewsContract.NewsEntry.CONTENT_URI,
             NewsContract.COLUMN_REQUIRED,
                NewsContract.NewsEntry.COLUMN_WIDGET + " = ? ",
                new String[] {"true"},
                null,
                null);



    }

    @Override
    public void onDestroy() {
        if(mCursor != null){
            mCursor = null;
        }

    }


    @Override
    public int getCount() {
        if(mCursor == null) return 0;
        return 1;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public RemoteViews getViewAt(int position) {
        if (!mCursor.moveToPosition(mCursor.getCount() - 1))
            return null;
        String typee = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_TYPE));
        String description = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_DESCRIPTION));
        String author = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_AUTHOR));
        String title = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_TITLE));
        String publishedAt = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_PUBLISHEDAT));
        NewsClass fillData = new NewsClass(author , title , description , null , null,publishedAt , typee);
        Bundle extras = new Bundle();
        extras.putParcelable("currentNews" , fillData);
        Intent fillInIntent = new Intent();
        fillInIntent.putExtra("currentNews" ,fillData);



        String newsTitle = mCursor.getString(mCursor.getColumnIndex(NewsContract.NewsEntry.COLUMN_TITLE));
        RemoteViews views = new RemoteViews(mContext.getPackageName(), R.layout.newswidget);



        views.setTextViewText(R.id.descrip, newsTitle);
        views.setOnClickFillInIntent(R.id.descrip, fillInIntent);
        return views;
    }


    @Override
    public RemoteViews getLoadingView() {
        return new RemoteViews(mContext.getPackageName(), R.layout.newswidget);
    }

    @Override
    public int getViewTypeCount() {
        return 1; // Treat all items in the GridView the same
    }

    @Override
    public long getItemId(int i) {

        return i;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }
}
