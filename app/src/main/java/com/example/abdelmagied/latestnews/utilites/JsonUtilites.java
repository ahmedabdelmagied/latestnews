package com.example.abdelmagied.latestnews.utilites;

import android.content.ContentValues;

import com.example.abdelmagied.latestnews.models.NewsClass;
import com.example.abdelmagied.latestnews.provider.NewsContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.example.abdelmagied.latestnews.provider.NewsProvider.context;


/**
 * Created by abdelmagied on 20/11/17.
 */

public class JsonUtilites {
    public static ArrayList<NewsClass> readJsonData(String myJsonString, String CategoryType) throws JSONException, ExecutionException, InterruptedException {

        if (myJsonString != "") {

            JSONObject root = new JSONObject(myJsonString);
            JSONArray articles = root.getJSONArray("articles");
            ArrayList<NewsClass> allNews = new ArrayList<>();
            context.getContentResolver().delete(NewsContract.NewsEntry.CONTENT_URI, NewsContract.NewsEntry.COLUMN_TYPE + " = ? ", new String[]{CategoryType});
            for (int i = 0; i < articles.length(); i++) {

                JSONObject myArticle = articles.getJSONObject(i);
                NewsClass currentNews = buildNewsObject(myArticle, CategoryType);
                allNews.add(currentNews);
            }
            return allNews;
        }
        return null;

    }


    public static NewsClass buildNewsObject(JSONObject currentNews, String categoryType) throws JSONException, ExecutionException, InterruptedException {

        String author = currentNews.getString("author");
        String title = currentNews.getString("title");
        String description = currentNews.getString("description");
        String url = currentNews.getString("url");
        String urlToImage = currentNews.getString("urlToImage");
        String publishedAt = currentNews.getString("publishedAt");

        ContentValues contentValues = new ContentValues();
        contentValues.put(NewsContract.NewsEntry.COLUMN_AUTHOR, author);
        contentValues.put(NewsContract.NewsEntry.COLUMN_DESCRIPTION, description);
        contentValues.put(NewsContract.NewsEntry.COLUMN_TITLE, title);
        contentValues.put(NewsContract.NewsEntry.COLUMN_URLTOIMAGE, urlToImage);
        contentValues.put(NewsContract.NewsEntry.COLUMN_TYPE, categoryType);
        contentValues.put(NewsContract.NewsEntry.COLUMN_PUBLISHEDAT, publishedAt);

        context.getContentResolver().insert(NewsContract.NewsEntry.CONTENT_URI, contentValues);


        return new NewsClass(author, title, description, url, urlToImage, publishedAt, categoryType);
    }
}
