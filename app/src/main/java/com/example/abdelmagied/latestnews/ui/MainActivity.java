package com.example.abdelmagied.latestnews.ui;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.abdelmagied.latestnews.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends AppCompatActivity {
    private static final int RC_SIGN_IN = 123;
    @BindView(R.id.loginEmail)
    EditText emailField;
    @BindView(R.id.loginPassword)
    EditText passwordField;
    @BindView(R.id.btnlogin)
    Button login;
    @BindView(R.id.btnSignUp)
    Button signUp;
    @BindView(R.id.skipButton)
    Button skip;
    public static FirebaseAuth firebaseAuth;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabaseReference;
    private String email = "";
    private String password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            String email = savedInstanceState.getString("email");
            String password = savedInstanceState.getString("password");
            emailField.setText(email);
            passwordField.setText(password);
        }

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.getInstance().signOut();

        if (firebaseAuth.getCurrentUser() != null) {
            Intent intent = new Intent(this, NewsActivity.class);
            startActivity(intent);
        }
        if (hasNetwork()) {
            login.setOnClickListener(new View.OnClickListener() {


                @Override
                public void onClick(View view) {
                    email = emailField.getText().toString();
                    password = passwordField.getText().toString();
                    if (email.equals("") || password.equals("")) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.error1), Toast.LENGTH_LONG).show();

                    } else {
                        firebaseAuth.signInWithEmailAndPassword(email, password)
                                .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {
                                            String name = firebaseAuth.getCurrentUser().getDisplayName();
                                            Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
                                            startActivity(intent);
                                            finish();
                                        } else {
                                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error2), Toast.LENGTH_LONG).show();
                                        }
                                    }

                                });
                    }

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.error3), Toast.LENGTH_LONG).show();
        }
        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivity(intent);
            }
        });

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), NewsActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("email", email);
        outState.putString("password", password);

    }

    public boolean hasNetwork() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }
}
