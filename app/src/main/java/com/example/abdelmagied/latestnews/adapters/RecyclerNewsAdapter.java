package com.example.abdelmagied.latestnews.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abdelmagied.latestnews.models.NewsClass;
import com.example.abdelmagied.latestnews.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abdelmagied on 20/11/17.
 */

public class RecyclerNewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<NewsClass> data;
    private ClickHandler mClickHandler;
    private int HEADER_VIEW_TYPE = 1;
    private int BODY_VIEW_TYPE = 2;

    public RecyclerNewsAdapter(Context context, ArrayList<NewsClass> data, ClickHandler mClickHandler) {
        this.context = context;
        this.data = data;
        this.mClickHandler = mClickHandler;
    }

    public void swapData(ArrayList<NewsClass> newData) {
        this.data = newData;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == HEADER_VIEW_TYPE) {
            view = LayoutInflater.from(context).inflate(R.layout.header_layout, parent, false);
            return new ViewHolderHeader(view);
        } else if (viewType == BODY_VIEW_TYPE) {
            view = LayoutInflater.from(context).inflate(R.layout.body_layout, parent, false);
            return new ViewBodyHolder(view);
        } else {
            return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        NewsClass mNewsClass = data.get(position);
        int type = getItemViewType(position);
        if (type == HEADER_VIEW_TYPE) {

            ViewHolderHeader holderHeader = (ViewHolderHeader) holder;
            holderHeader.data.setText(mNewsClass.getPublishedAt());

            if (data.get(position).getUrlToImage() == null) {
                Picasso.with(context)
                        .load(R.drawable.back)
                        .into(holderHeader.image);
            } else {
                Picasso.with(context)
                        .load(data.get(position).getUrlToImage())
                        .into(holderHeader.image);
            }

            holderHeader.title.setText(mNewsClass.getTitle());

        } else if (type == BODY_VIEW_TYPE) {

            ViewBodyHolder holderBody = (ViewBodyHolder) holder;
            holderBody.data.setText(mNewsClass.getPublishedAt());
            //set image
            if (data.get(position).getUrlToImage() == null) {
                Picasso.with(context)
                        .load(R.drawable.back)
                        .into(holderBody.image);
            } else {
                Picasso.with(context)
                        .load(data.get(position).getUrlToImage())
                        .into(holderBody.image);
            }
            holderBody.title.setText(mNewsClass.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        if (data == null) {
            return 0;
        }
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return HEADER_VIEW_TYPE;
        }
        return BODY_VIEW_TYPE;
    }

    public interface ClickHandler {
        void onItemClick(int id);
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.imageHId)
        public ImageView image;
        @BindView(R.id.dataHID)
        public TextView data;
        @BindView(R.id.titleHId)
        public TextView title;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mClickHandler.onItemClick(getAdapterPosition());
        }
    }


    public class ViewBodyHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.imageBId)
        public ImageView image;
        @BindView(R.id.dataBId)
        public TextView data;
        @BindView(R.id.titleBId)
        public TextView title;

        public ViewBodyHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mClickHandler.onItemClick(getAdapterPosition());
        }
    }
}
