package com.example.abdelmagied.latestnews.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by abdelmagied on 22/11/17.
 */

public class NewsDB extends SQLiteOpenHelper {

    public NewsDB(Context context) {

        super(context, NewsContract.NEWS_DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String DATABASE_QUERY = "CREATE TABLE " + NewsContract.NewsEntry.ROOT_TABLE_NAME + " ( " +
                NewsContract.NewsEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT  , " +
                NewsContract.NewsEntry.COLUMN_AUTHOR + " TEXT , " +
                NewsContract.NewsEntry.COLUMN_TITLE + " TEXT ," +
                NewsContract.NewsEntry.COLUMN_URLTOIMAGE + " TEXT , " +
                NewsContract.NewsEntry.COLUMN_PUBLISHEDAT + " TEXT , " +
                NewsContract.NewsEntry.COLUMN_TYPE + " TEXT ," +
                NewsContract.NewsEntry.COLUMN_WIDGET +  " TEXT , " +
                NewsContract.NewsEntry.COLUMN_DESCRIPTION + " TEXT )";

        sqLiteDatabase.execSQL(DATABASE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + NewsContract.NewsEntry.ROOT_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
