package com.example.abdelmagied.latestnews.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.abdelmagied.latestnews.models.Comments;
import com.example.abdelmagied.latestnews.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abdelmagied on 28/11/17.
 */

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {
    public Context context;
    ArrayList<Comments> data;

    public CommentsAdapter(Context context, ArrayList<Comments> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.comment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, int position) {
        Comments currentComment = data.get(position);
        holder.comment.setText(currentComment.comment);
        holder.name.setText(currentComment.userName);
    }

    @Override
    public int getItemCount() {
        if (data == null) return 0;
        return data.size();
    }

    public void swap(ArrayList<Comments> newData) {
        this.data = newData;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.nameId)
        public TextView name;
        @BindView(R.id.comment)
        public TextView comment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
