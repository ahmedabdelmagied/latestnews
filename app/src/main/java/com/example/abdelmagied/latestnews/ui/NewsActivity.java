package com.example.abdelmagied.latestnews.ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.example.abdelmagied.latestnews.adapters.RecyclerNewsAdapter;
import com.example.abdelmagied.latestnews.models.NewsClass;
import com.example.abdelmagied.latestnews.provider.NewsContract;
import com.example.abdelmagied.latestnews.R;
import com.example.abdelmagied.latestnews.utilites.NetworkUtilites;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.abdelmagied.latestnews.provider.NewsContract.COLUMN_REQUIRED;

public class NewsActivity extends AppCompatActivity implements RecyclerNewsAdapter.ClickHandler, LoaderManager.LoaderCallbacks<Cursor> {

    public static final String ApiKey = "b658229bf4d14a0f8bf7d2db1f61fa01";
    private int NEW_LOADER = 1;
    ArrayList<NewsClass> myData;
    @BindView(R.id.newsRecyclerID)
    RecyclerView mRecyclerView;
    RecyclerNewsAdapter mRecyclerNewsAdapter;
    ArrayList<NewsClass> LoaderData;
    public int recyclerPosition = 0;



     Toolbar mToolBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        mToolBar = (Toolbar) findViewById(R.id.activityBar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setTitle("");



        if (savedInstanceState != null) {
            recyclerPosition = savedInstanceState.getInt("recyclerViewPosition");
        }
        if (hasNetwork()) {
            String[] categories = getResources().getStringArray(R.array.categories);
            for (int i = 0; i < categories.length; i++) {
                try {
                    NetworkUtilites.getSpecificNews(categories[i], ApiKey);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }


        LoaderData = new ArrayList<>();
        try {
            LoaderData = NetworkUtilites.getSpecificNews("business", ApiKey);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (LoaderData == null) {
            Bundle bundle = new Bundle();
            bundle.putString("category", "business");
            getLoaderManager().restartLoader(NEW_LOADER, bundle, this);
        }

        mRecyclerNewsAdapter = new RecyclerNewsAdapter(this, LoaderData, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mRecyclerNewsAdapter);
        mRecyclerView.getLayoutManager().scrollToPosition(recyclerPosition);



    }

    public boolean hasNetwork() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    @Override
    public void onItemClick(int id) {
        NewsClass currentData = LoaderData.get(id);
        Intent intent = new Intent(this, NewsDetailsActivity.class);
        intent.putExtra("currentNews", currentData);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.categorymenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        LoaderData = new ArrayList<>();
        String categoryType = null;
        if (item.getItemId() == R.id.busId) {
            categoryType = "business";
        } else if (item.getItemId() == R.id.sport) {
            categoryType = "sport";
        } else if (item.getItemId() == R.id.tech) {
            categoryType = "technology";
        }else if(item.getItemId() == R.id.enter){
            categoryType = "entertainment";
        }else if(item.getItemId() == R.id.general){
            categoryType = "general";
        }else if(item.getItemId() == R.id.health){
            categoryType = "health";
        }else if(item.getItemId() == R.id.science){
            categoryType = "science";
        }

        try {
            LoaderData = NetworkUtilites.getSpecificNews(categoryType, ApiKey);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (LoaderData == null) {
            Bundle bundle = new Bundle();
            bundle.putString("category", categoryType);
            getLoaderManager().restartLoader(NEW_LOADER, bundle, this);
        }
        mRecyclerNewsAdapter.swapData(LoaderData);
        return true;
    }


    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        if (i == NEW_LOADER) {
            String categoryType = bundle.getString("category");
            return new CursorLoader(this,
                    NewsContract.NewsEntry.CONTENT_URI,
                    COLUMN_REQUIRED,
                    NewsContract.NewsEntry.COLUMN_TYPE + " = ? ",
                    new String[]{categoryType},
                    null);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        LoaderData = new ArrayList<>();
        if (data != null) {
            for (int i = 0; i < data.getCount(); i++) {
                data.moveToPosition(i);
                String typee = data.getString(data.getColumnIndex(NewsContract.NewsEntry.COLUMN_TYPE));
                String description = data.getString(data.getColumnIndex(NewsContract.NewsEntry.COLUMN_DESCRIPTION));
                String author = data.getString(data.getColumnIndex(NewsContract.NewsEntry.COLUMN_AUTHOR));
                String title = data.getString(data.getColumnIndex(NewsContract.NewsEntry.COLUMN_TITLE));
                String publishedAt = data.getString(data.getColumnIndex(NewsContract.NewsEntry.COLUMN_PUBLISHEDAT));

                LoaderData.add(new NewsClass(author, title, description, null, null, publishedAt, typee));
            }
        }
        mRecyclerNewsAdapter.swapData(LoaderData);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("recyclerViewPosition", ((LinearLayoutManager) mRecyclerView.getLayoutManager()).findFirstVisibleItemPosition());
    }
}
