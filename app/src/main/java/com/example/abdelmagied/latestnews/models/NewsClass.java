package com.example.abdelmagied.latestnews.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by abdelmagied on 20/11/17.
 */

public class NewsClass implements Parcelable {

    public String author;
    public String title;
    public String description;
    public String url;
    public String urlToImage;
    public String publishedAt;
    public String categoryType;

    public NewsClass(String author, String title, String description, String url, String urlToImage, String publishedAt, String categoryType) {
        this.author = author;
        this.title = title;
        this.description = description;
        this.url = url;
        this.urlToImage = urlToImage;
        this.publishedAt = publishedAt;
        this.categoryType = categoryType;
    }

    protected NewsClass(Parcel in) {
        author = in.readString();
        title = in.readString();
        description = in.readString();
        url = in.readString();
        urlToImage = in.readString();
        publishedAt = in.readString();
        categoryType = in.readString();
    }

    public static final Creator<NewsClass> CREATOR = new Creator<NewsClass>() {
        @Override
        public NewsClass createFromParcel(Parcel in) {
            return new NewsClass(in);
        }

        @Override
        public NewsClass[] newArray(int size) {
            return new NewsClass[size];
        }
    };


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public String getPublishedAt() {
        return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
        this.publishedAt = publishedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(author);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(url);
        parcel.writeString(urlToImage);
        parcel.writeString(publishedAt);
        parcel.writeString(categoryType);
    }
}
