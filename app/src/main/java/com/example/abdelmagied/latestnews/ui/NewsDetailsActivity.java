package com.example.abdelmagied.latestnews.ui;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.abdelmagied.latestnews.adapters.CommentsAdapter;
import com.example.abdelmagied.latestnews.models.Comments;
import com.example.abdelmagied.latestnews.models.NewsClass;
import com.example.abdelmagied.latestnews.provider.NewsContract;
import com.example.abdelmagied.latestnews.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.abdelmagied.latestnews.provider.NewsProvider.context;

public class NewsDetailsActivity extends AppCompatActivity {
    public FirebaseDatabase mFirebaseDatabase;
    public DatabaseReference mDatabaseReference;
    public FirebaseAuth mFirebaseAuth;
    public ArrayList<Comments> commentsData;
    public CommentsAdapter mRecyclerAdapter;
    @BindView(R.id.detailAuthorId)
    TextView author;
    @BindView(R.id.detailButtonId)
    Button linkButton;
    @BindView(R.id.detailImageId)
    ImageView NewsImage;
    @BindView(R.id.detailTitleId)
    TextView title;
    @BindView(R.id.detailDetailsId)
    TextView details;
    @BindView(R.id.commentsRecyclerId)
    RecyclerView commentsRecyclerView;
    @BindView(R.id.commentButtonId)
    Button comment;
    @BindView(R.id.commentText)
    EditText commentText;

    @BindView(R.id.commentSectionId)
    LinearLayout mLinearLayout;
    public String invaildFirebaseCharacters = ".#$[]";

    public int recyclerPosition = 0;
    private String myText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        ButterKnife.bind(this);
        if (savedInstanceState != null) {
            commentText.setText(savedInstanceState.getString("comment"));
            recyclerPosition = savedInstanceState.getInt("recyclerPosition");
        }
        final NewsClass currentData = (NewsClass) getIntent().getParcelableExtra("currentNews");


        mFirebaseAuth = FirebaseAuth.getInstance();
        author.setText(currentData.getAuthor());
        title.setText(currentData.getTitle());
        details.setText(currentData.getDescription());
        if (currentData.getUrlToImage() == null) {
            Picasso.with(this).load(R.drawable.back).into(NewsImage);
        } else {
            Picasso.with(this).load(currentData.getUrlToImage()).into(NewsImage);
        }

        linkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Uri web = Uri.parse(currentData.getUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, web);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }

            }
        });

        mRecyclerAdapter = new CommentsAdapter(this, commentsData);
        commentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        commentsRecyclerView.setAdapter(mRecyclerAdapter);
        commentsRecyclerView.getLayoutManager().scrollToPosition(recyclerPosition);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        String firebaseTitle = removeInvaildCharacters(currentData.getTitle());
        mDatabaseReference = mFirebaseDatabase.getReference().child("comments").child(firebaseTitle);

        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                commentsData = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {

                    commentsData.add(dataSnapshot1.getValue(Comments.class));
                    mRecyclerAdapter.swap(commentsData);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        comment.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                myText = commentText.getText().toString();
                Comments comment;
                if (mFirebaseAuth.getCurrentUser() != null) {
                    comment = new Comments(myText, mFirebaseAuth.getCurrentUser().getEmail());
                } else {
                    comment = new Comments(myText, "user");
                }
                mDatabaseReference.push().setValue(comment);
                commentText.setText("");
            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("comment", myText);
        outState.putInt("recyclerPosition", ((LinearLayoutManager) commentsRecyclerView.getLayoutManager()).findFirstVisibleItemPosition());
    }

    public String removeInvaildCharacters(String myStr) {
        myStr = myStr.replace(".", "");

        myStr = myStr.replace("#", "");
        myStr = myStr.replace("$", "");
        myStr = myStr.replace("[", "");
        myStr = myStr.replace("]", "");
        return myStr;
    }
}
