package com.example.abdelmagied.latestnews.ui;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RemoteViews;

import com.example.abdelmagied.latestnews.GridWidgetService;
import com.example.abdelmagied.latestnews.provider.NewsProvider;

import com.example.abdelmagied.latestnews.R;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {


    private static RemoteViews recipeGridRemoteView(Context context) {

        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        views.setOnClickPendingIntent(R.id.noWidget, pendingIntent);
        Intent secondIntent = new Intent(context, GridWidgetService.class);
        views.setRemoteAdapter(R.id.newsId, secondIntent);
        Intent recipeDetailsIntent = new Intent(context, NewsDetailsActivity.class);
        PendingIntent pendingTemplate = PendingIntent.getActivity(context, 0, recipeDetailsIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        views.setPendingIntentTemplate(R.id.newsId, pendingTemplate);
        views.setEmptyView(R.id.newsId, R.id.noWidget);
        return views;
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        for (int appWidgetId : appWidgetIds) {
            RemoteViews views = recipeGridRemoteView(context);
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }

    }


    @Override
    public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (NewsProvider.ACTION_UPDATA_NEWS_WIDGET.equals(intent.getAction())) {
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);

            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, NewAppWidget.class));
            appWidgetManager.notifyAppWidgetViewDataChanged(appWidgetIds, R.id.newsId);
        }
    }
}

