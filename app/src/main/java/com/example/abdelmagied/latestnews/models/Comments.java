package com.example.abdelmagied.latestnews.models;

/**
 * Created by abdelmagied on 27/11/17.
 */

public class Comments {

    public String comment;
    public String userName;

    public Comments() {
    }

    public Comments(String comment, String userName) {
        this.comment = comment;
        this.userName = userName;
    }
}
